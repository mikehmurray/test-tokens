pragma solidity ^0.4.11;

contract DistributeContract {
    address public owner;

    mapping (address => uint) participants;
    mapping (address => uint) amountDue;
    mapping (address => uint) amountPaid;

    // This needs to take an array of particpants but cannot get this to work yet
    function DistributeContract(address participantOne, address particpantTwo, uint dueSender, uint dueOne, uint dueTwo) public payable {
        owner = msg.sender;

        // Init participants
        participants[msg.sender] = 1;
        participants[participantOne] = 1;
        participants[particpantTwo] = 1;

        // Init amounts due for each participant
        amountDue[msg.sender] = dueSender;
        amountDue[participantOne] = dueOne;
        amountDue[particpantTwo] = dueTwo;
    }

    function payFunds() public payable returns (bool) {
        require(participants[msg.sender] != 0); // The sender must be a participant in the contract
        require(amountDue[msg.sender] > 0); // The sender must owe money on the contract
        require(amountDue[msg.sender] == msg.value); // The amount sent must be equal to the amount due

        amountDue[msg.sender] -= msg.value;
        amountPaid[msg.sender] += msg.value;
    }
}