const fs = require('fs');
const solc = require('solc');
const Web3 = require('web3');
const tokenName = 'DistributeContract';
const filePath = './DistributeContract.sol';

// Connect to local Ethereum node
//const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
const web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:8180"));

// Compile the source code
const input = fs.readFileSync(filePath);
const output = solc.compile(input.toString(), 1);
const bytecode = output.contracts[':' + tokenName].bytecode;
const abi = JSON.parse(output.contracts[':' + tokenName].interface);

// Contract object
const contract = web3.eth.contract(abi);

console.log('Accounts available: ', web3.eth.accounts)

// Deploy contract instance
const contractInstance = contract.new('0x82277a7700B1FC11Fa6fDdE3fDd4Df884c3E9e88', '0x1780ef53342711e7ecbf750e7f969553796f8f28', 1, 1, 1, {
    data: '0x' + bytecode,
    from: web3.eth.accounts[0],
    //from: '0x82277a7700B1FC11Fa6fDdE3fDd4Df884c3E9e88',
    gas: 90000*10
}, (err, res) => {
    if (err) {
        console.log(err);
        return;
    }

    // Log the tx, you can explore status with eth.getTransaction()
    console.log(res.transactionHash);

    // If we have an address property, the contract was deployed
    if (res.address) {
        console.log('Contract address: ' + res.address);
        // Let's test the deployed contract
        //testContract(res.address);
    } else{
        console.log('no address');
    }
});

function testContract(address) {
    // Reference to the deployed contract
    const token = contract.at(address);
    // Destination account for test
    const dest_account = '0x002D61B362ead60A632c0e6B43fCff4A7a259285';

    // Assert initial account balance, should be 100000
    const balance1 = token.totalSupply();
    console.log(balance1);
    const ress = token.changeTotal();
    const balance2 = token.totalSupply();
    console.log(balance2);

    // Call the transfer function
    // token.transfer(dest_account, 100, {from: web3.eth.coinbase}, (err, res) => {
    //     // Log transaction, in case you want to explore
    //     console.log('tx: ' + res);
    //     // Assert destination account balance, should be 100 
    //     const balance2 = token.balances.call(dest_account);
    //     console.log(balance2 == 100);
    // });
}