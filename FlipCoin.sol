pragma solidity ^0.4.18;

import './MintableToken.sol';

contract FlipCoin is MintableToken {
  string public name = "FLIP COIN";
  string public symbol = "FLP";
  uint8 public decimals = 18;
}